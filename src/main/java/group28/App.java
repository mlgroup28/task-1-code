package group28;

import group28.decisiontree.Node;
import group28.decisiontree.TDIT;
import group28.model.Row;
import group28.model.TableData;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.Paths;

/**
 * Hello world!
 */
public class App {

    private final static Logger logger = LoggerFactory.getLogger(App.class);

    // Parse data, build tree
    public static void main(String[] args) {
        if (args.length != 2) {
            logger.error("Usage group28.ex1.App <training_data.csv> <classification_data.csv>");
        } else {
            TableData trainingData = parseCSV(args[0]);
            TableData classificationData = parseCSV(args[1]);
            Node rootNode = new Node();
            rootNode.buildTree(trainingData.getHeaders(), trainingData.getRows());
            printTree(rootNode);
            printPrunedTree(rootNode);
            classify(classificationData,rootNode);
        }
    }

    // Classify given test data
    private static void classify(TableData classificationData, Node rootNode) {

        // Counters for Statistics
        int correct = 0;
        int correctPruned = 0;

        // Go through all of it
        for (Row r : classificationData.getRows()){
            boolean result = rootNode.classify(r);
            boolean resultPruned = rootNode.classifyPruned(r);
            if(result == r.classification()){
                correct++;
            }
            if(resultPruned == r.classification()){
                correctPruned++;
            }
        }

        double size = classificationData.getRows().size();
        logger.info("Accuracy: (correct={}) / total={}  = {}",correct, classificationData.getRows().size(), correct / size);
        logger.info("Accuracy (Pruned): (correct={}) / total={}  = {}",correctPruned, classificationData.getRows().size(), correctPruned / size);

    }

    private static void printTree(Node rootNode) {
        try {
            BufferedWriter writer = new BufferedWriter(new FileWriter("graph.dot"));
            writer.write("digraph Tree { \n");
            writer.write("node [shape=box, style=\"filled\", color=\"black\"] ;\n");
            writer.write(rootNode.toDigraph());
            writer.write("}\n");
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void printPrunedTree(Node rootNode) {
        try {
            BufferedWriter writer = new BufferedWriter(new FileWriter("graphPruned.dot"));
            writer.write("digraph Tree { \n");
            writer.write("node [shape=box, style=\"filled\", color=\"black\"] ;\n");
            writer.write(rootNode.toPrunedDigraph());
            writer.write("}\n");
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    /**
     * Parse a CSV file, retrieve its data
     * @param file
     * @return
     */
    private static TableData parseCSV(String file) {
        try {
            TableData tableData = new TableData();
            Reader reader = Files.newBufferedReader(Paths.get(file));
            CSVParser csvParser = new CSVParser(reader, CSVFormat.DEFAULT.withHeader());

            tableData.getHeaders().addAll(csvParser.getHeaderMap().keySet());
            for (CSVRecord csvRecord : csvParser){
                tableData.getRows().add(new Row(csvRecord.toMap()));
            }
            logger.info("Parsing '{}' - entries: {}",file, csvParser.getRecordNumber());

            return tableData;
        } catch (IOException e) {
            logger.error("Error parsing CSV file", e);
            System.exit(1);
        }
        return new TableData();
    }
}
