package group28.model;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;

public class Row {
    private HashMap<String, String> attributeValues = new HashMap<>();

    private boolean classification;

    private final static Logger logger = LoggerFactory.getLogger(Row.class);

    public Row(Map<String, String> valueMap) {
        for(String colName : valueMap.keySet()){
            if ("class_label".equals(colName)) {
                classification = Double.parseDouble(valueMap.get(colName)) == 1.0;
            } else {
                attributeValues.put(colName,valueMap.get(colName));
            }
        }

    }

    public Map<String, String> getAttributeValues() {
        return attributeValues;
    }

    public void setAttributeValues(HashMap<String, String> attributeValues) {
        this.attributeValues = attributeValues;
    }

    public double dblValue(String attr){
        return Double.parseDouble(attributeValues.get(attr));
    }

    @Override
    public String toString() {
        return "Row{" +
                "attributeValues=" + attributeValues +
                '}';
    }

    public boolean classification() {
        return classification;
    }
}
