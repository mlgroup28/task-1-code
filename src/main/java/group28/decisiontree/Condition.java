package group28.decisiontree;

import group28.model.Row;

public interface Condition {

    // Attribute name
    String label();

    // Apply this condition to a row
    boolean apply(Row data);

    // The conditional entropy while constructing the set
    double conditionalEntropy();
}
