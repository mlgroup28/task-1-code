package group28.decisiontree;

import group28.model.Row;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Comparator;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * The TDIT-algorithm - Contructs a tree
 */
public class TDIT {

    private final static Logger logger = LoggerFactory.getLogger(TDIT.class);

    public static Condition selectBestAttribute(Set<String> attributeNames, List<Row> examples) {
        //logger.info("Selecting the best attribute among {}",attributeNames);


        // Minimizing H(S) - H(S|A) is equivalent to finding the smallest H(S|A)
        double smallestCondtionalEntropy = Double.MAX_VALUE;
        Condition currentMin = null;
        for(String attr : attributeNames) {
            Condition c = createConditionFromAttribute(attr,examples);
            if(c.conditionalEntropy() < smallestCondtionalEntropy){
                currentMin = c;
                smallestCondtionalEntropy = c.conditionalEntropy();
            }
        }
        //logger.info("Best attribute is {} with conditional entropy {} ",currentMin,smallestCondtionalEntropy);

        return currentMin;
    }

    // First round, only doubles
    private static Condition createConditionFromAttribute(final String attr, final List<Row> examples) {
        // logger.info("Creating condition from attribute {}",attr);

        // Create a sorted List of all attribute values
        List sortedRows = examples.stream().sorted(
                Comparator.comparing(r -> r.dblValue(attr))
        ).collect(Collectors.toList());

        // For each adjacent examples with different class
        // Note: Starting at i=1, i-1 is always valid
        double minCondtionEntropy = Double.MAX_VALUE;
        Condition currentCondition = null; //new NumericCondition(0,attr,examples);
        for(int i = 1; i < sortedRows.size(); i++){
            Row currentRow = (Row) sortedRows.get(i);
            Row prevRow = (Row) sortedRows.get(i-1);

            // Got two adjacent examples with different classes?
            if(currentRow.classification() != prevRow.classification()) {
                // Construct split
                double mean = (currentRow.dblValue(attr) + prevRow.dblValue(attr)) / 2.0;
                NumericCondition condition = new NumericCondition(mean,attr,examples);
                if(condition.conditionalEntropy() < minCondtionEntropy) {
                    minCondtionEntropy = condition.conditionalEntropy();
                    currentCondition = condition;
//                    logger.debug("Thresholds {} minimizes the conditional entropy. Current value {}",mean,minCondtionEntropy);
                }
            }
        }
        return currentCondition;
    }

    // Calculate the conditional entropy (binary ouput)
    public static double H(double a, double b) {
        if(a == 0 || b == 0) {
            return 0;
        }

        return  a* log2(1/a) + b* log2(1/b);

    }

    // Helper method for log2
    private static double log2(double a){
        return Math.log(a) / Math.log(2);
    }

}
