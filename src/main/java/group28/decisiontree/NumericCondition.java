package group28.decisiontree;

import group28.model.Row;

import java.util.List;

public class NumericCondition implements Condition{
    private final double threshold;

    private final String attributeName;

    private final double conditionalEntropy;

    double belowPositive = 0;
    double belowNegative = 0;
    double abovePositive = 0;
    double aboveNegative = 0;

    double below = 0;
    double above = 0;


    public NumericCondition(double threshold, String attributeName, List<Row> examples) {
        this.threshold = threshold;
        this.attributeName = attributeName;
        this.conditionalEntropy = calculateCondtionalEntropy(threshold,attributeName,examples);
    }

    private double calculateCondtionalEntropy(double threshold, String attributeName, List<Row> examples){

        double total = examples.size();
        for (Row row : examples) {
            if(apply(row)){
                below++;
                if(row.classification()){
                    belowPositive++;
                } else {
                    belowNegative++;
                }
            } else {
                above++;
                if(row.classification()){
                    abovePositive++;
                } else {
                  aboveNegative++;
                }
            }

        }

        double pBelow = below / total;
        double pAbove = above / total;

        return pBelow * TDIT.H(belowPositive / below, belowNegative / below)
                + pAbove * TDIT.H(abovePositive / above, aboveNegative /above);
    }



    public boolean apply(Row data) {
        double value = Double.parseDouble(data.getAttributeValues().get(attributeName));
        return value <= threshold;
    }

    @Override
    public String toString() {
        return "NumericCondition{" +
                "threshold=" + threshold +
                ", attributeName='" + attributeName + '\'' +
                '}';
    }

    @Override
    public String label() {
        return attributeName + " &le; " + threshold;
    }


    @Override
    public double conditionalEntropy() {
        return conditionalEntropy;
    }


}
