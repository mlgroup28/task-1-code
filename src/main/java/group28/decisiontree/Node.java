package group28.decisiontree;

import group28.model.Row;
import group28.model.TableData;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Node {

    private static int nextId = 1;

    private static Logger logger = LoggerFactory.getLogger(Node.class);
    private int id = 0;

    private Node leftChild;
    private Node rightChild;

    private Condition condition;

    // Total Number of taken into account while constructing this node:
    private int samples = 0;


    int posNum = 0;
    int negNum = 0;
    List<Row> left = new ArrayList<>();
    List<Row> right = new ArrayList<>();

    private void buildTree(Set<String> attributes, List<Row> rows) {
        this.samples = rows.size();
        for(Row row: rows) {
            if(row.classification()) {
                posNum++;
            } else {
                negNum++;
            }
        }
        // logger.info("Selecting best attribute for: {} - postNum: {} - negNum: {}", id,posNum,negNum);
        if(posNum == 0 || negNum == 0){
            // logger.info("Node {} is perfectly classified pos: {} - neg {}",id,posNum,negNum);
        } else {
            this.condition = TDIT.selectBestAttribute(attributes,rows);
            // Taking care of child nodes
            for(Row row: rows) {
                if(condition.apply(row)){
                    left.add(row);
                } else {
                    right.add(row);
                }
            }
            leftChild = new Node();
            leftChild.id = nextId++;
            leftChild.buildTree(attributes,left);

            rightChild = new Node();
            rightChild.id = nextId++;
            rightChild.buildTree(attributes,right);
        }


    }

    public void buildTree(List<String> attributes, List<Row> rows) {
        Set<String> attrs = new HashSet<>();
        attrs.addAll(attributes);
        attrs.remove("class_label");
        buildTree(attrs,rows);

    }

    public boolean classify(Row r) {
        if(isLeaf()){
            return posNum > 0; // If this is a leaf, we classify it as "true", if there are only positive examples
        } else {
            if(condition.apply(r)){ // If the condition applies, continue left
                return leftChild.classify(r);
            } else { // Turn right
                return rightChild.classify(r);
            }
        }
    }

    public boolean classifyPruned(Row r) {
        return classifyPruned(r,3);
    }


     public boolean classifyPruned(Row r, int depth) {
        if(isLeaf() || isPrunedLeaf(depth)){
            return posNum >= negNum; // If this is a leaf, we classify it as "true", if there are only positive examples
        } else {
            if(condition.apply(r)){ // If the condition applies, continue left
                return leftChild.classifyPruned(r,depth - 1);
            } else { // Turn right
                return rightChild.classifyPruned(r, depth -1 );
            }
        }
    }

    private boolean isPrunedLeaf(int depth) {
        return isLeaf() || depth == 0;
    }


    public boolean isLeaf(){
        return condition == null;
    }

    public String toPrunedDigraph(){
        return toPrunedDigraph(3);
    }

    private String toPrunedDigraph(int i) {
        String leftInfo = "";
        String rightInfo = "";
        if (left.size() > 0 && !isPrunedLeaf(i)){
            leftInfo = String.format("%s -> %s  [labeldistance=2.5, labelangle=45, headlabel=\"True\"]; \n %s",id,leftChild.id,leftChild.toPrunedDigraph(i-1));
        }
        if(right.size() > 0 && !isPrunedLeaf(i)) {
            rightInfo = String.format("%s -> %s  [labeldistance=2.5, labelangle=45, headlabel=\"False\"]; \n %s",id,rightChild.id,rightChild.toPrunedDigraph(i-1));

        }

        String myInfo = "";
        if(isLeaf()) {
            myInfo = String.format("%s [label=<%s> fillcolor=\"#399de506\"] ;", id, (posNum >= negNum) ? "+" : "-");
        } else if(isPrunedLeaf(i)) {
            myInfo = String.format("%s [label=<%s (pruned) <br/> positive: %s, negative: %s> fillcolor=\"#399de506\"] ;", id, (posNum >= negNum) ? "+" : "-", posNum,negNum);

        } else {
            myInfo = String.format("%s [label=<samples = %s<br/> positive: %s, negative: %s <br/> condition: %s, <br/> conditional entropy: %s> fillcolor=\"#399de506\"] ;"
                    ,id,samples,posNum,negNum,condition.label(), condition.conditionalEntropy());
        }

        return myInfo + leftInfo + rightInfo;


    }

    public String toDigraph() {


        String leftInfo = "";
        String rightInfo = "";
        if (left.size() > 0){
            leftInfo = String.format("%s -> %s  [labeldistance=2.5, labelangle=45, headlabel=\"True\"]; \n %s",id,leftChild.id,leftChild.toDigraph());
        }
        if(right.size() > 0) {
            rightInfo = String.format("%s -> %s  [labeldistance=2.5, labelangle=45, headlabel=\"False\"]; \n %s",id,rightChild.id,rightChild.toDigraph());

        }

        String myInfo = String.format("%s [label=<%s> fillcolor=\"#399de506\"] ;",id,(posNum > 0) ? "+" : "-");

        if(!isLeaf()){
            myInfo = String.format("%s [label=<samples = %s<br/> positive: %s, negative: %s <br/> condition: %s, <br/> conditional entropy: %s> fillcolor=\"#399de506\"] ;"
                    ,id,samples,posNum,negNum,condition.label(), condition.conditionalEntropy());

        }

        return myInfo + leftInfo + rightInfo;
    }
}
