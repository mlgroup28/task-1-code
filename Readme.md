# Group 28 - Pracitical Assigment Sheet

This program implements the tdit algorithm for practical assigment task 1.
Java is used for solving the task.

For the group-depdendent task, group 28 is 28 mod 6 + 1 = 25 => Task 5 is implemented 

## Building
The Build is done using Apache Maven (https://maven.apache.org/)
* Compiling: `mvn compile`
* Creating an executable jar: `mvn package`
* Cleanup: `mvn clean`

## Running
Running creates (*and overwrites*) a file `graph.dot` in the current directory. 
Statistics are printed using the console.

* Using the executable jar: `java -jar target/ex1-1.0-SNAPSHOT-jar-with-dependencies.jar 
<path_to_training_data.csv> <path_to_test_data.cav>`. *Mind adjusting the path if you place it in a different folder*
* Using an IDE (IntelliJ)
  * Import the project as an Apache Maven project
  * Create a run configuration supplying paths to the CSV as arguments
  * Run `group28.App`


## Results
Are place at `results`:

* `results/independent` for the group independent task
* `results/dependent` for the group dependent task (Task 5.5)
* `results` contains the executeable jar


